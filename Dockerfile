FROM tiangolo/uwsgi-nginx-flask:flask


# upgrade pip and install required python packages
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential


# copy over our requirements.txt file
COPY requirements.txt /tmp/

RUN pip install -U pip
RUN pip install -r /tmp/requirements.txt

# copy over our app code
COPY ./app /app
