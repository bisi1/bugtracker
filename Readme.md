# Bug Tracker

Bug Tracker is an application which can be used to log bugs and defects in the project.
The code can be run on a dev laptop by directly executing the main.py script or by creating a docker image from the docker file provided.


## Downloading the project

Download the project using below given command.

```
git clone https://gitlab.com/bisi1/bugtracker
```


## Creating docker Image and running the application 

1. Go to bugtracker folder
```
cd bugtracker
```

2. If Docker not installed in your machine, please download and install Docker.

* https://www.docker.com/community-edition

3. Create Docker Image using below given command.
```
docker build -t bug_tracker .
```

4. Run Docker using below given command.
```
docker run -p 80:80 -t bug_tracker
```

5.Wait for 4-5 minutes for the web server to stat and start using Bug Tracker from below given link.

* http://0.0.0.0:80

## Run on Dev Laptop
1. Go to main folder
2. Install python in the laptop.
3. Go to bugtracker folder and install required libraries from requirements.txt file.
```
pip install -r requirements.txt
```

4. Change the port if required in main.py file in app folder.
5. Run the application using below command
```
python app/main.py
```

6. You can access the application using the ip and port. For example:
```
http://0.0.0.0:1212
```

## Improvement and Known Issues

There are know issues in the code which will nned to be fixed.

1. API to download all bugs.
2. Selnium scripts needs to be more comprehensive.
3. Unit test needs to be more comprehensive.
4. Delete user is not working.
5. A search option is required to search by name or bug id.
6. As functionalities get complex, use objects instead of list of tuples.