from time import strftime, gmtime

from flask import Flask, render_template, request

from bug_functions import list_all_bugs_query, delete_bug, add_bug, update_bug
from system_constants import BugEventKeys, UserEventKeys
from system_constants import all_priorities, all_bug_status, all_severities
from user_functions import list_all_user_query, add_user, update_user, delete_user
from database_conn import execute_select_statement

DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b3186b'

current_timestamp = strftime("%Y-%m-%d %H:%M:%S", gmtime())


@app.route("/", methods=['GET', 'POST', 'DELETE'])
def home():
    """ Main home page rendering function.
    Takes care of below functions of application:
      1. Shows list of bugs
      2. Add bugs
      3. Delete bugs
      4. Update bugs
    
    Returns:
        Returns web page template
    """
    ticket_status = request.args.get('status', 'New')
    ticket_number_to_update = request.form.get(BugEventKeys.TICKET_TO_UPDATE)
    ticket_number_to_delete = request.form.get(BugEventKeys.TICKET_TO_DELETE)

    if request.method == 'POST' and ticket_number_to_delete:
        return delete_bug(ticket_number_to_delete)

    elif request.method == 'POST' and ticket_number_to_update:
        return update_bug(request.form)

    elif request.method == 'POST':
        return add_bug(request.form)

    return render_template('home.html', page='home', status=ticket_status,
                           items=execute_select_statement(list_all_bugs_query.format(ticket_status)),
                           users=execute_select_statement(list_all_user_query), all_priorities=all_priorities,
                           all_bug_status=all_bug_status, all_severities=all_severities)


@app.route("/Users", methods=['GET', 'POST'])
def users():
    """ Main users page rendering function.
    Takes care of below functions of application:
      1. Shows list of users
      2. Add users
      3. Delete users
      4. Update users

    Returns:
        Returns web page template
    """
    user_to_update = request.form.get(UserEventKeys.USER_TO_UPDATE)
    user_to_delete = request.form.get(UserEventKeys.USER_TO_DELETE)

    if request.method == 'POST' and user_to_delete:
        return delete_user(user_to_delete)

    elif request.method == 'POST' and user_to_update:
        return update_user(request.form)

    elif request.method == 'POST':
        return add_user(request.form)

    return render_template('users.html', page='users', items=execute_select_statement(list_all_user_query))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
