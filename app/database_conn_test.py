from unittest import TestCase

from flask import app
from hamcrest.core import *

from database_conn import execute_sql_statement
from main import *
from user_functions import insert_user_query, delete_user_query, check_user_query


class TestDatabaseConnection(TestCase):
    def test_insert(self):
        """
        Tests database insert
        """
        with app.app_context():
            execute_sql_statement(insert_user_query.format('t1', 't2', 't3'))

    def test_select(self):
        """
        Tests database select
        """
        with app.app_context():
            results = execute_select_statement(check_user_query.format('t1'))
            for row in results:
                assert_that(row[0], is_('t1'))
                assert_that(row[1], is_('t2'))
                assert_that(row[2], is_('t3'))

    def test_delete(self):
        """
        Tests database delete
        """
        with app.app_context():
            execute_sql_statement(delete_user_query.format('t1'))
