from flask import flash
from flask.helpers import url_for
from werkzeug.utils import redirect

from database_conn import execute_select_statement, execute_sql_statement
from system_constants import UserEventKeys

list_all_user_query = "SELECT username, " \
                      "       firstname," \
                      "       lastname, " \
                      "       userid " \
                      "FROM  user"

delete_user_query = "DELETE " \
                    "FROM  user " \
                    "WHERE username = '{}'"

insert_user_query = "INSERT INTO user (username," \
                    "                  firstname," \
                    "                  lastname)" \
                    "          VALUES ('{}'," \
                    "                  '{}'," \
                    "                  '{}');"

update_bug_query = "UPDATE user " \
                   "SET firstname = '{}'," \
                   "    lastname = '{}'," \
                   "    username = '{}'" \
                   "WHERE userid = '{}'"

check_user_query = "SELECT username," \
                   "       firstname," \
                   "       lastname, " \
                   "       userid " \
                   "FROM  user " \
                   "WHERE username = '{}'"


def get_user_field_values(request_form):
    """Returns all values related to user in the request form

    Args:
        request_form(ImmutableMultiDict): Details of user as request form

    Returns:
        tuple: returns firstname , lastname, username and user_to_update
    """
    firstname = request_form.get(UserEventKeys.FIRST_NAME)
    lastname = request_form.get(UserEventKeys.LAST_NAME)
    username = request_form.get(UserEventKeys.USERNAME)
    user_to_update = request_form.get(UserEventKeys.USER_TO_UPDATE)
    return firstname, lastname, username, user_to_update


def delete_user(user_to_delete):
    """Deletes user from database
    
    Args:
        user_to_delete(int): userid to delete

    Returns:
        web page to redirect after deletion
    """
    statement = delete_user_query.format(user_to_delete)
    execute_sql_statement(statement)
    flash("User {} deleted.".format(user_to_delete))
    return redirect(url_for('users'))


def add_user(request_form):
    """Adds user from database

    Args:
        request_form(ImmutableMultiDict): Details of user

    Returns:
        web page to redirect after deletion
    """

    firstname, lastname, username, _ = get_user_field_values(request_form)

    if not check_username_exists(username):
        execute_sql_statement(insert_user_query.format(username, firstname, lastname))
        flash("User with name {} created.".format(username))

    return redirect(url_for('users'))


def update_user(request_form):
    """Updates a user from database

    Args:
        request_form(ImmutableMultiDict): Details of user

    Returns:
        web page to redirect after deletion
    """
    firstname, lastname, username, user_to_update = get_user_field_values(request_form)

    execute_sql_statement(update_bug_query.format(firstname, lastname, username, user_to_update))

    flash("User with username {} update.".format(username))
    return redirect(url_for('users'))


def check_username_exists(username):
    """checks whether a username already exists

    Args:
        username: username to check

    Returns :
        (bool) true if user exists else false
    """
    results = execute_select_statement(check_user_query.format(username))

    if results:
        flash("User with username {} already exist.".format(username))
        return True
    return False
