from flask import flash, request
from flask.helpers import url_for
from werkzeug.utils import redirect

from database_conn import execute_sql_statement
from system_constants import BugEventKeys

list_all_bugs_query = "SELECT ticketnumber, " \
                      "       title, " \
                      "       description, " \
                      "       datecreated, " \
                      "       status," \
                      "       priority," \
                      "       severity, " \
                      "       ifnull((SELECT username " \
                      "               FROM user " \
                      "               WHERE user.userid = bug.userid),'') " \
                      "FROM  bug " \
                      "WHERE status = '{}' " \
                      "ORDER BY ticketnumber DESC"

delete_bug_query = "DELETE " \
                   "FROM  bug " \
                   "WHERE ticketnumber = '{}'"

insert_bug_query = "INSERT INTO bug (title," \
                   "                 description," \
                   "                 datecreated," \
                   "                 Priority," \
                   "                 Severity," \
                   "                 userid," \
                   "                 status)" \
                   "         VALUES ('{}'," \
                   "                 '{}'," \
                   "                 current_timestamp," \
                   "                 '{}'," \
                   "                 '{}'," \
                   "                 '{}'," \
                   "                 'New');"

update_bug_query = "UPDATE bug " \
                   "SET title = '{}', " \
                   "    description = '{}', " \
                   "    priority = '{}', " \
                   "    severity = '{}',  " \
                   "    userid = '{}', " \
                   "    status= '{}'" \
                   "WHERE ticketnumber = '{}'"


def get_bug_field_values(request_form):
    """Returns all values related to bug in the request form

    Args:
        request_form(ImmutableMultiDict): Details of bug as request form

    Returns:
        tuple: returns firstname , lastname, username and user_to_update
    """
    title = request_form.get(BugEventKeys.TITLE)
    title_description = request_form.get(BugEventKeys.TITLE_DESCRIPTION)
    severity = request_form.get(BugEventKeys.SEVERITY)
    priority = request_form.get(BugEventKeys.PRIORITY)
    assignee = request_form.get(BugEventKeys.ASSIGNEE)
    status = request_form.get(BugEventKeys.STATUS)
    ticket_number_to_update = request.form.get(BugEventKeys.TICKET_TO_UPDATE)
    return title, title_description, severity, priority, assignee, status, ticket_number_to_update


def delete_bug(ticketnumber_fordelete):
    """Deletes bug from database

    Args:
        ticketnumber_fordelete(int): bugid to delete

    Returns:
        web page to redirect after deletion
    """
    sql_statement = delete_bug_query.format(ticketnumber_fordelete)
    execute_sql_statement(sql_statement)

    flash("Bug id {} deleted.".format(ticketnumber_fordelete))

    return redirect(url_for('home'))


def add_bug(request_form):
    """Adds bug from database

    Args:
        request_form(ImmutableMultiDict): Details of bug

    Returns:
        web page to redirect after deletion
    """

    title, title_description, severity, priority, assignee, _, _ = get_bug_field_values(request_form)
    sql_statement = insert_bug_query.format(title, title_description, severity, priority, assignee)
    execute_sql_statement(sql_statement)

    flash(" New Bug ticket created.")

    return redirect(url_for('home'))


def update_bug(request_form):
    """Updates a bug from database

    Args:
        request_form(ImmutableMultiDict): Details of bug

    Returns:
        web page to redirect after deletion
    """

    title, title_description, severity, priority, assignee, status, ticket_number_to_update = get_bug_field_values(
        request_form)

    sql_statement = update_bug_query.format(title, title_description, severity, priority, assignee, status,
                                            ticket_number_to_update)
    execute_sql_statement(sql_statement)

    flash("Bug {} has been updated.".format(ticket_number_to_update))
    return redirect(url_for('home'))
