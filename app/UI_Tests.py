from random import *

from ansible.compat.tests import unittest
from selenium import webdriver

class CheckApplication(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

        self.browser.get('http://0.0.0.0:1212/')

    def test_header(self):

        assert 'Bug Tracker' in self.browser.title

        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[1]/div/nav/div/a')
        assert elem.text in 'Bug Tracker'

        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[1]/div/nav/ul/li[1]/a')
        assert elem.text in 'Home'

        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[1]/div/nav/ul/li[2]/a')
        assert elem.text in 'Users'


    def test_create_bug(self):

        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[2]/h3/div/button')
        elem.click()

        elem = self.browser.find_element_by_xpath('//*[@id="title"]')
        elem.send_keys('Add user Issue')

        elem = self.browser.find_element_by_xpath('//*[@id="titledescription"]')
        elem.send_keys('Add user functionality is not working as expected')

        elem = self.browser.find_element_by_xpath('//*[@id="create_bug"]/div/div/form/div[2]/div/div/button')
        elem.click()

        all_elem = self.browser.find_element_by_name('message')
        assert all_elem.text in 'New Bug ticket created.'


    def test_bug_list(self):
        assert 'Bug Tracker' in self.browser.title



        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[1]/div/nav/ul/li[2]/a')
        elem.click()

        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[1]/div/nav/div/a')
        assert elem.text in 'Bug Tracker'

        elem = self.browser.find_element_by_xpath('/html/body/div/div/div[2]/h3/div/button')
        elem.click()

        elem = self.browser.find_element_by_xpath('//*[@id="userid"]')
        elem.send_keys('bisi1{}'.format(random()))

        elem = self.browser.find_element_by_xpath('//*[@id="firstname"]')
        elem.send_keys('Biju')

        elem = self.browser.find_element_by_xpath('//*[@id="lastname"]')
        elem.send_keys('Siva')

        elem = self.browser.find_element_by_xpath('//*[@id="create_user"]/div/div/form/div[2]/div/div/button')
        elem.click()


    def tearDown(self):
        self.browser.close()


if __name__ == '__main__':
    unittest.main()