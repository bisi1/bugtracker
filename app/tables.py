import sqlite3

from system_constants import DATABASE_NAME


class DatabaseClient(object):
    """ Client to connect to database """

    def __init__(self, database_name):
        self.database_name = database_name
        self.connection = None

    def open_connection(self):
        self.connection = sqlite3.connect(self.database_name)

    def close_connection(self):
        self.connection.close()

    def db_commit(self):
        self.connection.commit()

    def execute_statement(self, statement):
        self.connection.execute(statement)

    def select_statement(self, statement):
        result = self.connection.execute(statement)
        return result


bug_db = DatabaseClient(DATABASE_NAME)
bug_db.open_connection()

st1 = '''CREATE TABLE bug (
    ticketnumber INTEGER PRIMARY KEY AUTOINCREMENT,
    title        TEXT,
    description  TEXT,
    datecreated  TEXT,
    priority     TEXT,
    severity     TEXT,
    userid       INTEGER REFERENCES user (userid) ON DELETE CASCADE,
    status       TEXT
);
'''

bug_db.select_statement(st1)

st2 = '''CREATE TABLE user (
    userid      INTEGER PRIMARY KEY AUTOINCREMENT,
    username    TEXT    UNIQUE,
    firstname   TEXT,
    lastname    TEXT,
    createddate TEXT
)'''

bug_db.select_statement(st2)

bug_db.db_commit()

bug_db.close_connection()
