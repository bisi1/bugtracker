from unittest import TestCase
from hamcrest import *
from werkzeug.datastructures import ImmutableMultiDict

from bug_functions import add_bug, delete_bug, update_bug, insert_bug_query

test_data = ImmutableMultiDict([('titledescription', u'asca'), ('priority', u'Major'), ('severity', u'Major'), ('title', u'acd')])

class BugDMLTest(TestCase):

    def test_add_bug(self):
        # add_bug(insert_bug_query.format('Test', 'Test Description', 'High', 'High', 'Biju'))
        add_bug(test_data)

    #
    # def test_update_bug(self):
    #     update_bug(statement = insert_bug_query.format('Test', 'Test Description', 'High', 'High', 'Biju'))
    #
    # def test_delete_bug(self):
    #     select_statement

