import sqlite3

from flask import g

from system_constants import DATABASE_NAME


def _get_db():
    """Gets a database connection
    
    Returns (connect):
        Connection object to database
    """
    database = getattr(g, 'database', None)
    if database is None:
        database = g.database = sqlite3.connect(DATABASE_NAME)
    return database


def _close_connection():
    """Closes a database connection

    Returns:
        None
    """
    database = getattr(g, 'database', None)
    if database is not None:
        database.close()


def execute_sql_statement(statement):
    """Executes dml/ddl sql statement in database
    
    Args:
        statement: sql statement to execute

    Returns:
        None
    """
    cur = _get_db().execute(statement)
    _get_db().commit()
    cur.close()


def execute_select_statement(statement):
    """Executes select sql statement in database

    Args:
        statement: sql statement to execute

    Returns (list):
        All rows returned by select query
    """
    cur = _get_db().execute(statement)
    results = cur.fetchall()
    cur.close()
    return results
