DATABASE_NAME = 'bugtracker.db'

all_priorities = ['Major', 'High', 'Medium', 'Minor']
all_severities = ['Major', 'High', 'Medium', 'Minor']
all_bug_status = ['New', 'Open', 'InProgress', 'Testing', 'Closed']


class _BugEventKeys(object):
    TITLE = 'title'
    TITLE_DESCRIPTION = 'titledescription'
    SEVERITY = 'severity'
    PRIORITY = 'priority'
    ASSIGNEE = 'assignee'
    STATUS = 'status'
    TICKET_TO_UPDATE = 'ticket_number_to_update'
    TICKET_TO_DELETE = 'ticket_number_to_delete'


BugEventKeys = _BugEventKeys()
del _BugEventKeys


class _UserEventKeys(object):
    USERNAME = 'userid'
    FIRST_NAME = 'firstname'
    LAST_NAME = 'lastname'
    USER_TO_UPDATE = 'user_to_update'
    USER_TO_DELETE = 'user_to_delete'
    USERID = 'userid'


UserEventKeys = _UserEventKeys()
del _UserEventKeys
